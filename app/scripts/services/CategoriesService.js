/**
 * Categories
 * Servicio que modela el comportamiento de lo relacionado a las categorías de materiales de instalación.
 *
 * @param $resource ngResource/resource A factory which creates a resource object that lets you interact with RESTful server-side data sources.
 * @param $q ng/q A service that helps you run functions asynchronously, and use their return values (or exceptions) when they are done processing.
 * @param API_AGUAGENTE constante que contiene info relacionada la URL de la API
 * @returns {{getAll: getAll, create: create, update: update, delete: delete}}
 * @constructor
 */
function Categories ($resource, $q, API_AGUAGENTE) {

	var resource = $resource(
		API_AGUAGENTE.URL+'/categories/:id_categories',
		{
			'id_categories': '@id_categories'
		},
		{
			getAll: {
				method: 'GET'
			},
			create: {
				method: 'POST'
			},
			update: {
				method: 'POST',
				transformRequest: function(data) {
					return angular.toJson({
						'_method': 'PUT',
						'name': data.name,
						'description': data.description
					});
				}
			},
			delete: {
				method: 'DELETE'
			}

		}
	);

	return  {

    /**
     * Devuelve todas las categorias de materiales de instalación
     */
		getAll: function() {

			return resource
				.getAll()
				.$promise
				.then(function (response) {

					var categories = response.response;

					for(var i in categories) {

						for(var p in categories[i].elements) {

							var byteCharacters = atob(categories[i].elements[p].image);

							var byteNumbers = new Array(byteCharacters.length);

							for (var k = 0; k < byteCharacters.length; k++) {

							    byteNumbers[k] = byteCharacters.charCodeAt(k);

							}

							categories[i].elements[p].image = new Blob(
								[(new Uint8Array(byteNumbers)).buffer], {
								type: categories[i].elements[p].type,
								encoding: 'utf-8'
							})

						}

					}
					return categories;
				});

		},

    /**
     * Crea una nueva categoría
     * @param payload
     */
		create: function(payload) {

			return resource
				.create(payload)
				.$promise
				.then(function (response) {

					return response.response;

				});

		},

    /**
     * Actualiza los datos de alguna categoría
     * @param payload
     */
		update: function(payload) {

			return resource
				.update(payload)
				.$promise
				.then(function (response) {

					return response.response;

				});

		},

    /**
     * Borra alguna categoría.
     * @param params
     */
		delete: function(params) {

			return resource
				.delete(params)
				.$promise
				.then(function (response) {

					return response.response;

				});

		}

	}

}

Categories.$inject = ['$resource', '$q', 'API_AGUAGENTE'];
