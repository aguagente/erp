/**
 * Contracts
 * Modela el comportamiento de los Contratos de los clientes
 *
 * @param $resource ngResource/resource A factory which creates a resource object that lets you interact with RESTful server-side data sources.
 * @param $q ng/q A service that helps you run functions asynchronously, and use their return values (or exceptions) when they are done processing.
 * @param API_AGUAGENTE constante que contiene info relacionada la URL de la API
 * @returns {{getReferredByClient: getReferredByClient, getPurchasedByClient: getPurchasedByClient, getAll: getAll}}
 * @constructor
 */
function Contracts ($resource, $q, API_AGUAGENTE) {

	var resource = $resource(
		API_AGUAGENTE.URL+'/contracts/:id',
		{
			id: '@id'
		},
		{
			getReferredByClient: {
				method: 'GET'
			},
			getPurchasedByClient: {
				method: 'GET'
			},
			getAll: {
				method: 'GET'
			}
		}
	);

	return  {

    /**
     * getReferredByClient
     *
     * @param params
     */
		getReferredByClient: function(params) {

			return resource
				.getReferredByClient(params)
				.$promise
				.then(function (response) {

					var response = response.response;

					for(var i in response) {

						response[i].client.deposit = parseFloat(response[i].client.deposit) / 100;
						response[i].client.installation_fee = parseFloat(response[i].client.installation_fee) / 100;
						response[i].client.monthly_fee = parseFloat(response[i].client.monthly_fee) / 100;

						response[i].client.group.deposit = parseFloat(response[i].client.group.deposit) / 100;
						response[i].client.group.installation_fee = parseFloat(response[i].client.group.installation_fee) / 100;
						response[i].client.group.monthly_fee = parseFloat(response[i].client.group.monthly_fee) / 100;
						/*
						if(response[i].paid_at !== null) {

			            	response[i]._status = moment().diff(moment(response[i].paid_at), 'months') === 0 ? 1 : 0;

			            } else {

			                response[i]._status = -1;
			            }*/

					}

					return response;

				});
		},

    /**
     * getPurchasedByClient
     *
     * @param params
     */
		getPurchasedByClient: function(params) {

			return resource
				.getPurchasedByClient(params)
				.$promise
				.then(function (response) {

					var response = response.response;

					for(var i in response) {

						response[i].client.deposit = parseFloat(response[i].client.deposit) / 100;
						response[i].client.installation_fee = parseFloat(response[i].client.installation_fee) / 100;
						response[i].client.monthly_fee = parseFloat(response[i].client.monthly_fee) / 100;

						response[i].client.group.deposit = parseFloat(response[i].client.group.deposit) / 100;
						response[i].client.group.installation_fee = parseFloat(response[i].client.group.installation_fee) / 100;
						response[i].client.group.monthly_fee = parseFloat(response[i].client.group.monthly_fee) / 100;
						/*
						if(response[i].paid_at !== null) {

			            	response[i]._status = moment().diff(moment(response[i].paid_at), 'months') === 0 ? 1 : 0;

			            } else {

			                response[i]._status = -1;
			            }*/

					}

					return response;

				});

		},

    /**
     * getAll
     * Devuelve todos los contratos.
     */
		getAll: function() {

			return resource
				.getAll()
				.$promise
				.then(function (response) {

					var response = response.response;

					for(var i in response) {

						response[i].client.deposit = parseFloat(response[i].client.deposit) / 100;
						response[i].client.installation_fee = parseFloat(response[i].client.installation_fee) / 100;
						response[i].client.monthly_fee = parseFloat(response[i].client.monthly_fee) / 100;

						response[i].client.group.deposit = parseFloat(response[i].client.group.deposit) / 100;
						response[i].client.group.installation_fee = parseFloat(response[i].client.group.installation_fee) / 100;
						response[i].client.group.monthly_fee = parseFloat(response[i].client.group.monthly_fee) / 100;
						/*
			            if(response[i].paid_at !== null) {

			            	response[i]._status = moment().diff(moment(response[i].paid_at), 'months') === 0 ? 1 : 0;

			            } else {

			                response[i]._status = -1;
			            }*/

					}

					return response;


				});

		}

	};

}

Contracts.$inject = ['$resource', '$q', 'API_AGUAGENTE'];
