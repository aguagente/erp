angular
    .module('aguagente-front-services', [])

.service('AuthService', AuthService)
    .service('SessionService', SessionService)
    .service('Users', Users)

.service('Clients', Clients)
    .service('Commissions', Commissions)
    .service('Cards', Cards)
    .service('Groups', Groups)
    .service('Contracts', Contracts)

.service('Categories', Categories)
    .service('Elements', Elements)

.service('Polls', Polls)

.service('ErrorCodes', ErrorCodes)
    .service('Tickets', Tickets)

.service('Geolocation', Geolocation)

.provider('showErrorsConfig', showErrorsConfig)
    .service('ResizeFileImage', ResizeFileImage)
    .service('Posts', Posts)
    .service('Imagen', Imagen)
    .service('PushNotifications', PushNotifications)
    .service('Roles', Roles)
    .service('FinanceClients', FinanceClientsService)
    .service('Debts', DebtsService)
    .service('Charges', ChargesService)
    .service('SubscriptionEvents', SubscriptionEventsService)
    .service('Dashboard', DashboardService)
    .service('Withdrawals', WithdrawalsService)
    .service('Invoices', Invoices);