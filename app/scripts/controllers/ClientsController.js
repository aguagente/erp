function ClientsController(
  $scope, $modal, $compile, $timeout, $filter,
  DTOptionsBuilder, DTColumnBuilder, DTColumnDefBuilder, Clients, Charges, Cards, Contracts, Groups, groups, API_AGUAGENTE) {

  $scope.data = {
    'clients': [],
    //'filteredClients': clients,
    'selectedAll': false,
    'searchStatus': {
      'status': '',
      'serial_number': '',
      'sales_agent': undefined
    },
    'dtInstance': {},
    'dtOptions': DTOptionsBuilder.newOptions()
      .withDOM('<"html5buttons"B>lTfgitp')
      .withButtons([{
        extend: 'excel',
        exportOptions: {
          columns: '.exportar'
        },
        text: 'Excel'
      },
        {
          extend: 'pdf',
          exportOptions: {
            columns: ':not(:last-child)'
          },
          text: 'PDF',
          title: 'Clientes'
        },
        {
          extend: 'print',
          exportOptions: {
            columns: ':not(:last-child)'
          },
          text: 'Imprimir',
          title: 'Clientes',
          customize: function (win) {
            $(win.document.body).addClass('white-bg');
            $(win.document.body).css('font-size', '10px');
            $(win.document.body).find('table')
              .addClass('compact')
              .css('font-size', 'inherit');
          }
        }
      ])
      .withOption('order', [])
      .withOption('createdRow', function (row, data, dataIndex) {
        $timeout(function () {
          $compile(angular.element(row).contents())($scope);
        });
      })
      .withOption('ajax', {
        url: API_AGUAGENTE.URL + '/clients/get_clients',
        type: 'GET',
        data: function (d) {
          d.status = $scope.data.searchStatus.status;
          d.serial_number = $scope.data.searchStatus.serial_number;
          d.sales_agent = $scope.data.searchStatus.sales_agent;
        },
        complete: function (jqXHR, textStatus) {
          if (jqXHR.status === 200) {
            $scope.data.clients = jqXHR.responseJSON.data;
          }
          $timeout(function () {
            $('#menuItemGroup').children().remove();
            $('#menuItemGroup').append('<a href="" ng-click="associateClientsToGroup((data.clients | filter:{selected: true}:true))"><i class="fa fa-arrow-right"></i>&nbsp;&nbsp;Asignar a grupo</a>')
            $compile(angular.element('#selectAllInput').contents())($scope);
            $compile(angular.element('#groupsButton').contents())($scope);
            $scope.selectedAll = false;
          });
        }
      })
      .withDataProp('data')
      .withOption('processing', true)
      .withOption('serverSide', true)
      .withPaginationType('full_numbers'),
    'dtColumns': [
      DTColumnBuilder.newColumn('id_clients').withOption("searchable", false).withOption("orderable", false).withTitle('<label id="selectAllInput" style="cursor:pointer;"><input type="checkbox" icheck iCheck-class="square-green" ng-model="selectedAll" ng-change="selectAll()"></label>').renderWith(function (data, type, client, meta) {
        var _html = '<label style="cursor:pointer;"><input icheck ng-model="data.clients[' + meta.row + '].selected" ng-change="changeClient(' + client.id_clients + ',data.clients[' + meta.row + '].selected)" type="checkbox" icheck iCheck-class="square-green"></label>';
        return _html;
      }),
      DTColumnBuilder.newColumn('name').withTitle('Nombre').renderWith(function (data, type, client) {
        var _rfcCaptured = (client.rfc ? true : false),
          _isInstalled = client.serial_number !== null,
          _isAgent = client.sales_agent == 1;

        var _html = client.name + "<br/>" +
          '<span class="h6">' +
          '<span class="text-' + (_rfcCaptured ? 'info' : 'warning') + '">Datos de facturación' + (_rfcCaptured ? '' : ' no') + ' capturados</span>' +
          '</span >' +
          '<div class="clearfix"></div>' +
          (_isAgent ? '<span class="label label-info">Agente de ventas</span>' : '') +
          '<div class="clearfix"></div>';

        if (_isInstalled) {
          _html += '<span class="h6">' +
            '<b>Número serie</b><br>' +
            '<i class="fa fa-tint fa-lg text-info fa-fw"></i >' +
            client.serial_number +
            '</span>';
        }
        return _html;
      }),
      DTColumnBuilder.newColumn('group').withTitle('Grupo de ventas').renderWith(function (data, type, client) {
        return client.group.name;
      }),
      DTColumnBuilder.newColumn('created_at').withTitle('Fecha alta').withClass('exportar').renderWith(function (data, type, client) {
        //console.log(moment(client.created_at).format('ddd DD MMM YYYY HH:mm:ss'));
        return moment(client.created_at).format('ddd DD MMM YYYY HH:mm:ss');
      }),
      DTColumnBuilder.newColumn('subscription_status').withTitle('Suscripción').withClass('exportar').renderWith(function (data, type, client) {
        var _cssClass = 'default',
          _text = 'Faltante';

        switch (client.subscription_status) {
          case "in_trial":
            _cssClass = 'primary';
            _text = 'Periodo de prueba';
            break;
          case "active":
            _cssClass = 'success';
            _text = 'Activa';
            break;
          case "paused":
            _cssClass = 'info';
            _text = 'Pausada';
            break;
          case "past_due":
            _cssClass = 'warning';
            _text = 'Fallando';
            break;
          case "canceled":
            _cssClass = 'danger';
            _text = 'Cancelada';
            break;
        }

        return '<span class="label label-' + _cssClass + '">' + _text + '</span>';
      }),
      DTColumnBuilder.newColumn('email').withTitle('Email').withClass('exportar'),
      DTColumnBuilder.newColumn('name').withTitle('Estatus').withClass('exportar').renderWith(function (data, type, client) {
        var _cssClass = 'default',
          _text = 'Pendiente',
          _isInstalled = client.serial_number !== null;

        switch (client.status) {
          case "invalid":
            _cssClass = 'warning';
            _text = 'Invalido';
            break;
          case "accepted":
            _cssClass = 'info';
            _text = 'Aceptado';
            if (_isInstalled) {
              _text = 'Instalado';
            }

            break;
          case "canceled":
            _cssClass = 'danger';
            _text = 'Cancelado';
            break;
          case "rejected":
            _cssClass = 'danger';
            _text = 'Rechazado';
            break;
        }

        return '<span class="label label-' + _cssClass + '">' + _text + '</span>';
      }),
      DTColumnBuilder.newColumn('next_payday').withTitle('Sig. Pago').withClass('exportar').renderWith(function (data, type, client) {
        //console.log(moment(client.created_at).format('ddd DD MMM YYYY HH:mm:ss'));
        return moment(client.next_payday).format('YYYY-MM-DD');
      }),
      DTColumnBuilder.newColumn(null).withTitle('<div id="groupsButton" class="row"><div class="btn-group btn-group-sm text-right" dropdown><button type="button" class="btn" style="width:100%;background-color: #1B7E5A;border-color: #1B7E5A;color:#fff;" ng-disabled="(data.clients | filter:{selected: true}:true).length == 0" dropdown-toggle><i class="fa fa-users fa-fw fa-2x"></i>&nbsp;&nbsp;&nbsp;&nbsp;<span class="fa fa-fw fa-caret-down"></span></button><ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="split-button"><li role="menuitem" id="menuItemGroup"></li></ul></div></div>').withOption("searchable", false).withOption("orderable", false).renderWith(function (data, type, client, meta) {
        //console.log(meta);
        var _html = "<div class='btn-group' dropdown>" +
          "<button type='button' class='btn btn-primary dropdown-toggle' data-toggle='dropdown'>" +
          "Opciones&nbsp;<span class='fa fa-fw fa-caret-down'></span>" +
          "</button>" +
          "<ul class='dropdown-menu dropdown-menu-right' role='menu' aria-labelledby='split-button'>" +
          "<li role='menuitem' ng-click='reviewPurchaseContract(" + JSON.stringify(client).replace(/'/g, '&#39') + ")'>" +
          "<a href=''>" +
          "<i class='fa fa-fw fa-file'></i>" +
          "Ver contrato de compra" +
          "</a>" +
          "</li>" +
          "<li role='menuitem' ng-click='reviewReferredContracts(" + JSON.stringify(client).replace(/'/g, '&#39') + ")'>" +
          "<a href=''>" +
          "<i class='fa fa-fw fa-files-o'></i>" +
          "Ver contratos referidos" +
          "</a>" +
          "</li>" +
          "<li class='divider'></li>" +
          "<li role='menuitem' ng-click='reviewValidationHistory(" + JSON.stringify(client).replace(/'/g, '&#39') + ")'>" +
          "<a href=''>" +
          "<i class='fa fa-fw fa-search'></i>" +
          "Historial de validación" +
          "</a>" +
          "</li>" +
          "<li class='divider'></li>" +
          "<li role='menuitem' ng-click='createCharge(" + JSON.stringify(client).replace(/'/g, '&#39') + ")'>" +
          "<a href=''>" +
          "<i class='fa fa-fw fa-usd'></i>" +
          "Crear cargo OXXO / SPEI" +
          "</a>" +
          "</li>" +
          "<li role='menuitem' ng-click='reviewCharges(" + JSON.stringify(client).replace(/'/g, '&#39') + ")'>" +
          "<a href=''>" +
          "<i class='fa fa-fw fa-usd'></i>" +
          "Ver cargos" +
          "</a>" +
          "</li>" +
          "<li class='divider'></li>" +
          "<li role='menuitem' ng-click='edit(" + JSON.stringify(client).replace(/'/g, '&#39') + ")'>" +
          "<a href=''>" +
          "<i class='fa fa-fw fa-pencil'></i>" +
          "Editar" +
          "</a>" +
          "</li>" +
          "<li class='divider'></li>" +
          "<li role='menuitem' ng-click='resetPassword(" + JSON.stringify(client).replace(/'/g, '&#39') + ")'>" +
          "<a href=''>" +
          "<i class='fa fa-fw fa-key'></i>" +
          "Reestablecer contraseña" +
          "</a>" +
          "</li>" +
          "<li class='divider'></li>";

        if (client.sales_agent == 1) {
          _html = _html + "<li role='menuitem' ng-click='setLevel(" + JSON.stringify(client).replace(/'/g, '&#39') + ",0)'>" +
            "<a href=''>Quitar de agente de ventas</a>" +
            "</li>";
        } else {
          _html = _html + "<li role='menuitem' ng-click='setLevel(" + JSON.stringify(client).replace(/'/g, '&#39') + ",1)'>" +
            "<a href=''>Activar como agente de ventas</a>" +
            "</li>";
        }

        if (client.status == 'standby') {
          _html = _html + "<li class='divider'></li><li role='menuitem' ng-click='setInvalid(" + JSON.stringify(client).replace(/'/g, '&#39') + ")'><a href=''>Invalidar cliente</a></li>" +
            "<li class='divider'></li> <li role='menuitem' ng-click='setRejected(" + JSON.stringify(client).replace(/'/g, '&#39') + ")'><a href=''>Rechazar cliente</a></li>"
        }

        if (client.status == 'standby' || client.status == 'rejected') {
          _html = _html + "<li class='divider'></li><li role='menuitem' ng-click='setAccepted(" + JSON.stringify(client).replace(/'/g, '&#39') + ")'><a href=''>Aceptar cliente</a></li>";
        }

        if (client.status == 'accepted') {
          _html = _html + "<li class='divider'></li><li role='menuitem' ng-click='setCancelled(" + JSON.stringify(client).replace(/'/g, '&#39') + ")'><a href=''>Cancelar cliente</a></li>";
        }

        if (client.status == 'rejected') {
          _html = _html + "<li class='divider'></li><li role='menuitem' ng-click='delete(" + JSON.stringify(client).replace(/'/g, '&#39') + ")'><a href=''><i class='fa fa-fw fa-trash fa-lg fa-fw'></i>Eliminar cliente</a></li>";
        }


        _html = _html + "<li class='divider'></li>" +
          "<li role='menuitem' ng-click='createTicket(" + JSON.stringify(client).replace(/'/g, '&#39') + ")'>" +
          "<a href=''>" +
          "<i class='fa fa-fw fa-ticket'></i>" +
          "Crear ticket de soporte" +
          "</a>" +
          "</li>" +
          "</ul>" +
          "</div>"
        return _html;
      })
    ]
  }

  $scope.$on('event:dataTablePage', function (event, loadedDT) {
    console.log('ho');
  });

  $scope.switchInvoice = function (idclient) {

    Clients
      .switchInvoice(idclient)
      .then(function (response) {

        swal({
          title: 'Cliente actualizado correctamente',
          text: '',
          type: 'success',
          confirmButtonColor: '#128f76',
          confirmButtonText: 'Aceptar'
        });

      })

  };

  $scope.selectAll = function () {
    angular.forEach($scope.data.clients, function (client, key) {
      client.selected = $scope.selectedAll;
    });
  };

  $scope.changeClient = function (id_clients, value) {
    var selectedAll = true;
    angular.forEach($scope.data.clients, function (client, key) {
      if (client.id_clients === id_clients) {
        client.selected = value;
      }
      if (!client.selected) {
        selectedAll = false;
      }
    });

    $scope.selectedAll = selectedAll;
  };

  $scope.associateClientsToGroup = function (clients) {

    $modal.open({

      templateUrl: 'views/client-associate-group.html',
      controllerAs: 'instance',
      size: 'lg',
      backdrop: 'static',
      keyboard: false,
      resolve: {

        clients: function () {

          return clients;

        },

        groups: function () {

          return Groups
            .getAll()
            .then(function (response) {
              return response;
            });

        }

      },
      controller: ['$filter', '$modalInstance', 'clients', 'groups', 'Groups',

        function ($filter, $modalInstance, clients, groups, Groups) {

          var instance = this;

          instance.data = {
            'clients': clients,
            'groups': groups,
            'group': null,
            'dtOptions': DTOptionsBuilder.newOptions()
              .withDOM('<"html5buttons"B>lTfgitp')
              .withButtons([{
                extend: 'excel',
                exportOptions: {
                  columns: ':not(:last-child)'
                },
                text: 'Excel'
              },
                {
                  extend: 'pdf',
                  exportOptions: {
                    columns: ':not(:last-child)'
                  },
                  text: 'PDF',
                  title: 'Clientes'
                },
                {
                  extend: 'print',
                  exportOptions: {
                    columns: ':not(:last-child)'
                  },
                  text: 'Imprimir',
                  title: 'Clientes',
                  customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');

                    $(win.document.body).find('table')
                      .addClass('compact')
                      .css('font-size', 'inherit');
                  }
                }
              ])
              .withOption('order', []),
            'dtColumnDefs': [
              DTColumnDefBuilder.newColumnDef(2).withOption("type", "date-nl")
            ]
          };

          instance.close = function () {

            $modalInstance.dismiss();

          }

          instance.associateClients = function (group, clients) {

            Groups
              .associateClients({
                'id_groups': group.id_groups,
                'id_clients': (function () {
                  return clients.map(function (client) {
                    return client.id_clients;
                  })

                })()
              })
              .then(function (response) {

                swal({

                  title: "Clientes asignados!",
                  type: "success",
                  showCancelButton: false,
                  confirmButtonColor: "#128f76",
                  confirmButtonText: "Continuar",
                  closeOnConfirm: true

                }, function () {
                  $scope.reloadClientsTable();
                  $modalInstance.close({
                    'group': group,
                    'clients': clients
                  });

                });

              });

          }

        }
      ]
    })
      .result.then(function (data) {

      for (var i in data.clients) {

        data.clients[i].group = data.group;
        data.clients[i].selected = false;

        $scope.data.clients[$scope.data.clients.indexOf(data.clients[i])] = data.clients[i];

      }

    })
      .catch(function () {
      });

  }

  $scope.edit = function (client) {
    $modal.open({

      templateUrl: 'views/client-edit.html',
      controllerAs: 'instance',
      size: 'lg',
      backdrop: 'static',
      keyboard: false,
      resolve: {

        client: function () {

          return angular.copy(client);

        }

      },
      controller: ['$filter', '$modalInstance', 'FileUploader', 'ResizeFileImage', 'client', 'Clients',

        function ($filter, $modalInstance, FileUploader, ResizeFileImage, client, Clients) {

          var instance = this;

          instance.data = {
            'client': client,
            'states': [
              'Distrito Federal',
              'Aguascalientes',
              'Baja California',
              'Baja California Sur',
              'Campeche',
              'Chiapas',
              'Chihuahua',
              'Coahuila',
              'Colima',
              'Durango',
              'Guanajuato',
              'Guerrero',
              'Hidalgo',
              'Jalisco',
              'Estado de México',
              'Michoacán',
              'Morelos',
              'Nayarit',
              'Nuevo León',
              'Oaxaca',
              'Puebla',
              'Querétaro',
              'Quintana Roo',
              'San Luis Potosí',
              'Sinaloa',
              'Sonora',
              'Tabasco',
              'Tamaulipas',
              'Tlaxcala',
              'Veracruz',
              'Yucatán',
              'Zacatecas'
            ]
          };

          instance.data.client.invoice = instance.data.client.invoice_data;

          var filterImage = {
            name: 'imageFilter',
            fn: function (item /*{File|FileLikeObject}*/, options) {
              var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
              return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
            }
          };

          var failedImage = function (item, filter, options) {
            swal(
              "Error!",
              "El archivo " + item.name + " no es una imagen",
              "error"
            );

          };

          instance.idUploader = new FileUploader({
            url: '',
            alias: 'id'
          });

          instance.idUploader.filters.push(filterImage);
          instance.idUploader.onWhenAddingFileFailed = failedImage;
          instance.idUploader.onAfterAddingFile = function (fileItem) {

            ResizeFileImage.photo(fileItem._file, 1200, 'file', function (file) {
              instance.idUploader.queue[instance.idUploader.getIndexOfItem(fileItem)]._file = file;
            });

          };

          instance.id_reverseUploader = new FileUploader({
            url: '',
            alias: 'address'
          });

          instance.id_reverseUploader.filters.push(filterImage);
          instance.id_reverseUploader.onWhenAddingFileFailed = failedImage;
          instance.id_reverseUploader.onAfterAddingFile = function (fileItem) {

            ResizeFileImage.photo(fileItem._file, 1200, 'file', function (file) {
              instance.id_reverseUploader.queue[instance.id_reverseUploader.getIndexOfItem(fileItem)]._file = file;
            });

          };

          instance.addressUploader = new FileUploader({
            url: '',
            alias: 'address'
          });

          instance.addressUploader.filters.push(filterImage);
          instance.addressUploader.onWhenAddingFileFailed = failedImage;
          instance.addressUploader.onAfterAddingFile = function (fileItem) {

            ResizeFileImage.photo(fileItem._file, 1200, 'file', function (file) {
              instance.addressUploader.queue[instance.addressUploader.getIndexOfItem(fileItem)]._file = file;
            });

          };

          instance.profileUploader = new FileUploader({
            url: '',
            alias: 'address'
          });

          instance.profileUploader.filters.push(filterImage);
          instance.profileUploader.onWhenAddingFileFailed = failedImage;
          instance.profileUploader.onAfterAddingFile = function (fileItem) {

            ResizeFileImage.photo(fileItem._file, 1200, 'file', function (file) {
              instance.profileUploader.queue[instance.profileUploader.getIndexOfItem(fileItem)]._file = file;
            });

          };

          instance.close = function () {

            $modalInstance.dismiss();

          }

          instance.editClient = function (client) {
            client.id = instance.idUploader.queue.length ? instance.idUploader.queue[0]._file : '';
            client.id_reverse = instance.id_reverseUploader.queue.length ? instance.id_reverseUploader.queue[0]._file : '';
            client.proof_address = instance.addressUploader.queue.length ? instance.addressUploader.queue[0]._file : '';
            client.profile = instance.profileUploader.queue.length ? instance.profileUploader.queue[0]._file : '';

            if (instance.data.client.invoice) {
              client.invoice_data = JSON.stringify(instance.data.client.invoice);
            }

            Clients
              .update(client)
              .then(function (response) {
                $scope.reloadClientsTable();
                swal({

                  title: "Datos de cliente actualizados correctamente",
                  type: "success",
                  showCancelButton: false,
                  confirmButtonColor: "#128f76",
                  confirmButtonText: "Continuar",
                  closeOnConfirm: true

                }, function () {

                  $modalInstance.close(client);

                });

              });

          }

        }
      ]
    })
      .result.then(function (editedClient) {

      editedClient.status = client.status === 'invalid' ? 'standby' : client.status;
      $scope.data.clients[$scope.data.clients.indexOf(client)] = editedClient;

    });

  }

  $scope.setLevel = function (client, nivel) {

    swal({

      title: "Estás seguro de cambiar el nivel del cliente " + client.name + "?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#128f76",
      confirmButtonText: "Si, continuar.",
      closeOnConfirm: false

    }, function () {

      Clients
        .setLevel({
          'id_clients': client.id_clients,
          'level': nivel
        })
        .then(function (response) {

          client.sales_agent = nivel;

          swal({
            title: "Operación realizada correctamente",
            type: "success",
            confirmButtonText: "Continuar",
            confirmButtonColor: "#2196F3",
            allowOutsideClick: true
          }, function () {
          });

        });

    });
  }

  $scope.resetPassword = function (client) {

    $modal.open({

      templateUrl: 'views/client-reset-password.html',
      controllerAs: 'instance',
      size: 'lg',
      backdrop: 'static',
      keyboard: false,
      resolve: {
        'client': function () {
          return client;
        }
      },
      controller: ['$scope', '$filter', '$modalInstance', 'Users', 'client',

        function ($scope, $filter, $modalInstance, Users, client) {

          var instance = this;

          client.password = '';
          client.checkPassword = '';

          instance.data = {
            'user': client,
            'name': client.name,
            'email': client.email,
            'loading': false
          }

          instance.close = function () {

            $modalInstance.dismiss();

          }

          instance.save = function (form, user) {

            if (form.$valid && user.password === user.checkPassword && user.password.length >= 6) {

              instance.data.loading = true;

              Users
                .update({
                  'id': user.id_users,
                  'name': instance.data.name,
                  'email': instance.data.email,
                  'password': user.password
                })
                .then(function (response) {

                  swal({
                    title: 'Cliente actualizado correctamente',
                    text: '',
                    type: 'success',
                    confirmButtonColor: '#128f76',
                    confirmButtonText: 'Aceptar'
                  }, function () {

                    instance.data.loading = false;
                    $modalInstance.close(response);

                  });

                })


            } else {

              $scope.$broadcast('show-errors-check-validity');

              swal({
                title: 'Un momento',
                text: "Favor de completar los datos obligatorios.",
                type: "error",
                confirmButtonColor: "#128f76",
                confirmButtonText: "Aceptar"
              });

            }

          }

        }
      ]
    });

  }

  $scope.reviewPurchaseContract = function (client) {
    console.log(client);
    $modal.open({

      templateUrl: 'views/client-review-purchase-contract.html',
      controllerAs: 'instance',
      size: 'lg',
      backdrop: 'static',
      keyboard: false,
      resolve: {

        refereer: function () {
          return client.referred_by ? Clients.get({'id_clients': client.referred_by}) : null;
        },

        contract: function () {
          return Contracts
            .getPurchasedByClient({'id_clients': client.id_clients})
            .then(function (contracts) {
              if (contracts.length !== 0)
                return contracts[0];
              else
                return null;
            });
        },

        cards: function () {
          return Cards.getAll({'id_clients': client.id_clients})
        }

      },
      controller: [
        '$filter', '$timeout', '$modalInstance',
        'refereer', 'contract', 'cards',

        function (
          $filter, $timeout, $modalInstance,
          refereer, contract, cards) {

          var instance = this;

          instance.data = {
            'client': client,
            'refereer': refereer,
            'contract': contract,
            'cards': cards,
            'rotateLabels': {
              rotateLeft: ' (rotate left) ',
              rotateRight: ' (rotate right) ',
              zoomIn: ' (zoomIn) ',
              zoomOut: ' (zoomOut) ',
              fit: ' (fit) ',
            },
            'editable': true,
            'loading': false
          }

          instance.close = function () {

            $modalInstance.dismiss();

          }

          instance.setInvalid = function (client) {

            swal({
              title: "Motivos",
              text: "Favor de proporcionar las razones por las que el cliente será invalidado",
              type: "input",
              showCancelButton: true,
              closeOnConfirm: false,
              confirmButtonText: "Continuar",
              confirmButtonColor: "#23c6c8",
              cancelButtonText: "Cancelar",
              animation: "slide-from-top",
            }, function (reasons) {

              if (reasons === false) return false;
              if (reasons === "") {
                swal.showInputError("Debes escribir los motivos.");
                return false;
              }

              Clients
                .setStatus({
                  'id_clients': client.id_clients,
                  'status': 'invalid',
                  'reasons': reasons
                })
                .then(function (response) {
                  swal({
                    title: "Operación realizada correctamente",
                    type: "success",
                    confirmButtonText: "Continuar",
                    confirmButtonColor: "#2196F3",
                    allowOutsideClick: true
                  }, function () {
                    $modalInstance.close(response);
                  });
                });

            });

          }

          instance.setAccepted = function (client) {

            swal({

              title: "Estás seguro de aceptar al cliente ?",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#128f76",
              confirmButtonText: "Si, continuar.",
              closeOnConfirm: false

            }, function () {

              Clients
                .setStatus({
                  'id_clients': client.id_clients,
                  'status': 'accepted'
                })
                .then(function (response) {

                  swal({
                    title: "Operación realizada correctamente",
                    type: "success",
                    confirmButtonText: "Continuar",
                    confirmButtonColor: "#2196F3",
                    allowOutsideClick: true
                  }, function () {
                    $modalInstance.close(response);
                  });

                });

            });

          }

          instance.setCancelled = function (client) {

            swal({
              title: "Motivos",
              text: "Favor de proporcionar las razones por las que el cliente será cancelado",
              type: "input",
              showCancelButton: true,
              closeOnConfirm: false,
              confirmButtonText: "Continuar",
              confirmButtonColor: "#23c6c8",
              cancelButtonText: "Cancelar",
              animation: "slide-from-top",
            }, function (reasons) {

              if (reasons === false) return false;
              if (reasons === "") {
                swal.showInputError("Debes escribir los motivos.");
                return false
              }

              Clients
                .setStatus({
                  'id_clients': client.id_clients,
                  'status': 'canceled',
                  'reasons': reasons
                })
                .then(function (response) {

                  swal({
                    title: "Operación realizada correctamente",
                    type: "success",
                    confirmButtonText: "Continuar",
                    confirmButtonColor: "#2196F3",
                    allowOutsideClick: true,
                  }, function () {
                    $modalInstance.close(response);
                  });

                });

            });

          }

          instance.setRejected = function (client) {

            swal({
              title: "Motivos de rechazo",
              text: "Favor de proporcionar las razones por las que el cliente será rechazado",
              type: "input",
              showCancelButton: true,
              closeOnConfirm: false,
              confirmButtonText: "Continuar",
              confirmButtonColor: "#23c6c8",
              cancelButtonText: "Cancelar",
              animation: "slide-from-top",
            }, function (reasons) {

              if (reasons === false) return false;
              if (reasons === "") {
                swal.showInputError("Debes escribir los motivos!");
                return false
              }

              Clients
                .setStatus({
                  'id_clients': client.id_clients,
                  'status': 'rejected',
                  'reasons': reasons
                })
                .then(function (response) {

                  swal({
                    title: "Operación realizada correctamente",
                    type: "success",
                    confirmButtonText: "Continuar",
                    confirmButtonColor: "#2196F3",
                    allowOutsideClick: true
                  }, function () {
                    $modalInstance.close(response);
                  });

                });

            });

          }

          instance.collectFirstPayment = function (client) {

            swal({
              title: "Atención!",
              text: "Estás seguro de continuar? se intentará recolectar el primer pago del cliente.",
              type: "warning",
              showCancelButton: true,
              confirmButtonText: "Si, continuar.",
              confirmButtonColor: "#23c6c8",
              cancelButtonText: "Cancelar",
              closeOnConfirm: false
            }, function () {

              instance.data.loading = true;

              Clients
                .subscribeToPlan({
                  'id_clients': client.id_clients
                })
                .then(function (response) {

                  instance.data.loading = false;
                  swal({
                    title: "Operación realizada correctamente",
                    type: "success",
                    confirmButtonText: "Continuar",
                    confirmButtonColor: "#2196F3",
                    allowOutsideClick: true
                  });

                }, function () {
                  instance.data.loading = false;
                });

            });


          }

          instance.collectFirstPaymentOffline = function (client) {
            $total = instance.data.contract.client.deposit;
            $totalForSr = 0;
            if (instance.data.contract.client.group.trial_days_price !== '0.00') {
              $total += instance.data.contract.client.group.trial_days_price;
              $totalForSr += instance.data.contract.client.group.trial_days_price;
            } else {
              $total += instance.data.contract.client.monthly_fee;
              $totalForSr += instance.data.contract.client.monthly_fee;
            }

            angular.forEach(instance.data.contract.elements, function (item, key) {
              console.log(item);
              if (item.element_name == 'Acero inoxidable') {
                $total += parseInt(instance.data.contract.client.installation_fee);
                $totalForSr += parseInt(instance.data.contract.client.installation_fee);
              } else {
                $total += parseInt(item.price);
                $totalForSr += parseInt(item.price);
              }
            });

            if (instance.data.contract.client.social_responsability == 1) {
              _sr = ($totalForSr / 1.16) * .007;
              $total += _sr;
            }

            $scope.createCharge(client, $total);
          }
        }
      ]
    })
      .result.then(function (changedClient) {

      $scope.data.clients[$scope.data.clients.indexOf(client)] = changedClient;

    });

  }

  $scope.reviewReferredContracts = function (client) {
    console.log(client);
    $modal.open({

      templateUrl: 'views/client-review-referred-contracts.html',
      controllerAs: 'instance',
      size: 'lg',
      backdrop: 'static',
      keyboard: false,
      resolve: {

        contracts: function () {

          return Contracts
            .getReferredByClient({
              'referred_by': client.id_clients
            });

        },

        hierarchicalTree: function () {

          return Clients
            .getHierarchicalTree({
              'id_clients': client.id_clients
            })
            .then(function (response) {
              return $filter('toArray')(response);
            })
        },

        comissions: function () {

          return Clients
            .getComissions({
              'id_clients': client.id_clients
            })
        },

        loosingComissions: function () {

          return Clients
            .getLoosingComissions({
              'id_clients': client.id_clients
            })
        },

        clientsScope: function () {

          return $scope;

        }

      },
      controller: ['$filter', '$modalInstance', 'contracts', 'hierarchicalTree', 'comissions', 'loosingComissions', 'clientsScope',

        function ($filter, $modalInstance, contracts, hierarchicalTree, comissions, loosingComissions, clientsScope) {

          var instance = this;

          instance.formatForAngularTree = function (tree) {

            tree = {
              'id': tree.id_clients,
              'client': tree,
              'nodes': $filter('toArray')(tree.clients) || []
            }

            for (var i in tree.nodes) {

              tree.nodes[i] = instance.formatForAngularTree(tree.nodes[i]);

            }

            return tree;

          }

          instance.reviewImages = function (contract) {

            $('#blueimp-gallery').data('use-bootstrap-modal', true);

            blueimp.Gallery(
              (function (documents) {
                var images = [];
                for (var i in documents) {
                  images.push({
                    'href': documents[i]
                  });
                }
                return images;
              })(contract.documents.concat(contract.photos)),
              $('#blueimp-gallery').data()
            );

          }

          instance.reviewPurchaseContract = function (client) {

            clientsScope.reviewPurchaseContract(client);

          }

          instance.changeSelectedNode = function (client) {
            instance.data.selectedNode = client;
          }

          instance.close = function () {

            $modalInstance.dismiss();

          }

          instance.getComissionsResponse = function (comissions) {
            var niveles;
            comissions.forEach(function (i, v) {
              if (i != 'isActive') {
                niveles[i] = v;
              }
            });
            //console.log(niveles);
            return niveles;
          }

          var hierarchicalTree = [
            instance.formatForAngularTree(hierarchicalTree[0])
          ];

          instance.data = {
            'client': client,
            'hierarchicalTree': hierarchicalTree,
            'commissions': comissions,
            'loosingComissions': loosingComissions,
            'selectedNode': hierarchicalTree[0].client,
            'contracts': contracts,
            'dtOptions': DTOptionsBuilder.newOptions()
              .withDOM('<"html5buttons"B>lTfgitp')
              .withButtons([{
                extend: 'excel',
                exportOptions: {
                  columns: ':not(:last-child)'
                },
                text: 'Excel'
              },
                {
                  extend: 'pdf',
                  exportOptions: {
                    columns: ':not(:last-child)'
                  },
                  text: 'PDF',
                  title: 'Contratos'
                },
                {
                  extend: 'print',
                  exportOptions: {
                    columns: ':not(:last-child)'
                  },
                  text: 'Imprimir',
                  title: 'Contratos',
                  customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');

                    $(win.document.body).find('table')
                      .addClass('compact')
                      .css('font-size', 'inherit');
                  }
                }
              ]),
            'dtColumnDefs': [
              DTColumnDefBuilder.newColumnDef(9).notSortable(),
              DTColumnDefBuilder.newColumnDef(1).withOption("type", "date-nl"),
              DTColumnDefBuilder.newColumnDef(5).withOption("type", "date-nl")
            ]

          }

          console.log();
        }
      ]
    });

  }

  $scope.reviewValidationHistory = function (client) {

    $modal.open({

      templateUrl: 'views/client-review-validation-history.html',
      controllerAs: 'instance',
      size: 'lg',
      backdrop: 'static',
      keyboard: false,
      resolve: {

        validationHistory: function () {

          return Clients
            .getValidationHistory({
              'id_clients': client.id_clients
            })
            .then(function (response) {

              var history = [];

              history = history
                .concat([{
                  'created': client.id_clients,
                  'reasons': 'Creado',
                  'created_at': client.created_at
                }])
                .concat(response.invalidations)
                .concat(response.cancellations)
                .concat(response.corrections)
                .concat(response.rejections);

              if (client.status === 'accepted') {

                history = history.concat([{
                  'accepted': client.id_clients,
                  'reasons': 'Aceptado',
                  'created_at': client.updated_at

                }]);
              }

              return history;

            })

        }

      },
      controller: ['$filter', '$modalInstance', 'validationHistory',

        function ($filter, $modalInstance, validationHistory) {

          var instance = this;

          instance.data = {
            'client': client,
            'validationHistory': validationHistory
          }

          instance.close = function () {

            $modalInstance.dismiss();

          }

          instance.dateDiff = function (date) {

            if ((days = moment().diff(moment.utc(date).toDate(), 'days')) > 0)
              return days + ' dias';
            else if ((hours = moment().diff(moment.utc(date).toDate(), 'hours')) > 0)
              return hours + ' horas';
            else if ((minutes = moment().diff(moment.utc(date).toDate(), 'minutes')) > 0)
              return minutes + ' minutos';
            else if ((seconds = moment().diff(moment.utc(date).toDate(), 'seconds')) > 0)
              return seconds + ' segundos';

          }

        }
      ]
    });

  }

  $scope.reviewCharges = function (client) {

    $modal.open({

      templateUrl: 'views/client-review-charges.html',
      controllerAs: 'instance',
      size: 'lg',
      backdrop: 'static',
      keyboard: false,
      resolve: {

        charges: function () {

          return Clients
            .getCharges({
              'id_clients': client.id_clients
            });

        }

      },
      controller: ['$filter', '$modalInstance', '$modal', 'charges',

        function ($filter, $modalInstance, $modal, charges) {

          var instance = this;

          instance.data = {
            'client': client,
            'charges': charges.filter(function (charge) {
              return charge.paid_at != null;
            }),
            'dtOptions': DTOptionsBuilder.newOptions()
              .withDOM('<"html5buttons"B>lTfgitp')
              .withButtons([{
                extend: 'excel',
                exportOptions: {
                  columns: ':not(:last-child)'
                },
                text: 'Excel'
              },
                {
                  extend: 'pdf',
                  exportOptions: {
                    columns: ':not(:last-child)'
                  },
                  text: 'PDF',
                  title: 'Contratos'
                },
                {
                  extend: 'print',
                  exportOptions: {
                    columns: ':not(:last-child)'
                  },
                  text: 'Imprimir',
                  title: 'Devoluciones',
                  customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');

                    $(win.document.body).find('table')
                      .addClass('compact')
                      .css('font-size', 'inherit');
                  }
                }
              ]),
            'dtColumnDefs': [
              DTColumnDefBuilder.newColumnDef(5).notSortable(),
              DTColumnDefBuilder.newColumnDef(2).withOption("type", "date-nl")
            ]
          }

          instance.close = function () {

            $modalInstance.dismiss();

          }

          instance.refund = function (charge) {

            $modal.open({
              templateUrl: 'views/client-review-charges-make-refund.html',
              controllerAs: 'instance',
              size: 'sm',
              backdrop: 'static',
              keyboard: false,
              resolve: {
                client: function () {
                  return client;
                },
                charge: function () {
                  return charge;
                }
              },
              controller: ['$scope', '$modalInstance', 'Clients', 'client', 'charge',

                function ($scope, $modalInstance, Clients, client, charge) {

                  var instance = this;

                  instance.data = {
                    client: client,
                    charge: charge,
                    refund: {
                      id_charges: charge.id_charges,
                      amount: 0
                    },
                    loading: false
                  }

                  instance.makeRefund = function (form, data) {

                    if (form.$valid) {

                      Clients
                        .refundCharge({
                          id_charges: charge.id_charges,
                          amount: data.amount * 100
                        })
                        .then(function (response) {

                          swal({
                            title: "Operación realizada correctamente",
                            text: "",
                            type: "success",
                            confirmButtonText: "Continuar",
                            confirmButtonColor: "#2196F3",
                            allowOutsideClick: true
                          });

                          $modalInstance.close(response);

                        });

                    } else {

                      $scope.$broadcast('show-errors-check-validity');

                    }

                  }

                  instance.dismiss = function () {

                    $modalInstance.dismiss();

                  }
                }
              ]
            })
              .result.then(function (updatedCharge) {
              instance.data.charges[instance.data.charges.indexOf(charge)] = updatedCharge;
            });

          }

        }
      ]
    });


  }

  $scope.setInvalid = function (client) {

    swal({
      title: "Motivos",
      text: "Favor de proporcionar las razones por las que el cliente será invalidado",
      type: "input",
      showCancelButton: true,
      closeOnConfirm: false,
      confirmButtonText: "Continuar",
      confirmButtonColor: "#23c6c8",
      cancelButtonText: "Cancelar",
      animation: "slide-from-top",
    }, function (reasons) {

      if (reasons === false) return false;

      if (reasons === "") {
        swal.showInputError("Debes escribir los motivos.");
        return false
      }

      Clients
        .setStatus({
          'id_clients': client.id_clients,
          'status': 'invalid',
          'reasons': reasons
        })
        .then(function (response) {

          client.status = 'invalid';

          swal({
            title: "Operación realizada correctamente",
            text: "",
            type: "success",
            confirmButtonText: "Continuar",
            confirmButtonColor: "#2196F3",
            allowOutsideClick: true
          });
          $scope.reloadClientsTable();
        });

    });

  }

  $scope.setAccepted = function (client) {

    swal({

      title: "Estás seguro de aceptar al cliente ?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#128f76",
      confirmButtonText: "Si, continuar.",
      closeOnConfirm: false

    }, function () {

      Clients
        .setStatus({
          'id_clients': client.id_clients,
          'status': 'accepted'
        })
        .then(function (response) {

          client.status = 'accepted';

          swal({
            title: "Operación realizada correctamente",
            text: "",
            type: "success",
            confirmButtonText: "Continuar",
            confirmButtonColor: "#2196F3",
            allowOutsideClick: true
          });
          $scope.reloadClientsTable();
        });

    });

  }

  $scope.setCancelled = function (client) {

    swal({
      title: "Motivos",
      text: "Favor de proporcionar las razones por las que el cliente será cancelado",
      type: "input",
      showCancelButton: true,
      closeOnConfirm: false,
      confirmButtonText: "Continuar",
      confirmButtonColor: "#23c6c8",
      cancelButtonText: "Cancelar",
      animation: "slide-from-top",
    }, function (reasons) {

      if (reasons === false) return false;

      if (reasons === "") {
        swal.showInputError("Debes escribir los motivos.");
        return false
      }

      Clients
        .setStatus({
          'id_clients': client.id_clients,
          'status': 'canceled',
          'reasons': reasons
        })
        .then(function (response) {

          client.status = 'canceled';

          swal({
            title: "Operación realizada correctamente",
            text: "",
            type: "success",
            confirmButtonText: "Continuar",
            confirmButtonColor: "#2196F3",
            allowOutsideClick: true
          });
          $scope.reloadClientsTable();
        });

    });

  }

  $scope.setRejected = function (client) {

    swal({
      title: "Motivos de rechazo",
      text: "Favor de proporcionar las razones por las que el cliente será rechazado",
      type: "input",
      showCancelButton: true,
      closeOnConfirm: false,
      confirmButtonText: "Continuar",
      confirmButtonColor: "#23c6c8",
      cancelButtonText: "Cancelar",
      animation: "slide-from-top",
    }, function (reasons) {

      if (reasons === false) return false;

      if (reasons === "") {
        swal.showInputError("Debes escribir los motivos!");
        return false
      }

      Clients
        .setStatus({
          'id_clients': client.id_clients,
          'status': 'rejected',
          'reasons': reasons
        })
        .then(function (response) {

          client.status = 'rejected';

          swal({
            title: "Operación realizada correctamente",
            text: "",
            type: "success",
            confirmButtonText: "Continuar",
            confirmButtonColor: "#2196F3",
            allowOutsideClick: true
          });
          $scope.reloadClientsTable();
        });

    });

  }

  $scope.makeSalesAgent = function (client) {

    swal({
      title: "Atención",
      text: "¿ Estás seguro de hacer agente de ventas al cliente ?",
      type: "warning",
      showCancelButton: true,
      closeOnConfirm: false,
      confirmButtonText: "Continuar",
      confirmButtonColor: "#23c6c8",
      cancelButtonText: "Cancelar",
      animation: "slide-from-top",
    }, function () {

      Clients
        .makeSalesAgent({
          'id_clients': client.id_clients
        })
        .then(function (response) {

          swal({
            title: "Operación realizada correctamente",
            text: "",
            type: "success",
            confirmButtonText: "Continuar",
            confirmButtonColor: "#2196F3",
            allowOutsideClick: true
          });
          $scope.reloadClientsTable();
        });

    });

  }

  $scope.delete = function (client) {

    swal({
      title: "Atención",
      text: "¿ Estás seguro de eliminar al cliente ?",
      type: "warning",
      showCancelButton: true,
      closeOnConfirm: false,
      confirmButtonText: "Continuar",
      confirmButtonColor: "#23c6c8",
      cancelButtonText: "Cancelar",
      animation: "slide-from-top",
    }, function () {

      Clients
        .delete({
          'id_clients': client.id_clients
        })
        .then(function (response) {

          $scope.data.filteredClients.splice(
            $scope.data.filteredClients.indexOf(client),
            1
          );

          swal({
            title: "Operación realizada correctamente",
            text: "",
            type: "success",
            confirmButtonText: "Continuar",
            confirmButtonColor: "#2196F3",
            allowOutsideClick: true
          });

        });

    });

  }

  $scope.createTicket = function (client) {

    $modal.open({

      templateUrl: 'views/client-create-ticket.html',
      controllerAs: 'instance',
      size: 'lg',
      backdrop: 'static',
      keyboard: false,
      resolve: {

        client: function () {

          return client;

        },

        errorCodes: ['ErrorCodes', function (ErrorCodes) {

          return ErrorCodes.getAll();

        }]

      },
      controller: [
        '$scope', '$filter', '$timeout', '$modalInstance',
        'Tickets',
        'client', 'errorCodes',

        function (
          $scope, $filter, $timeout, $modalInstance,
          Tickets,
          client, errorCodes) {

          var instance = this;

          instance.data = {
            'ticket': {
              'id_clients': client.id_clients,
              'error_code': '',
              'description': '',
              'estimated_service_fee': '',
              'estimated_service_fee_reasons': '',
              'type': '',
              'status': 'opened'
            },
            'client': client,
            'errorCodes': errorCodes,
            'loading': false
          }

          instance.close = function () {

            $modalInstance.close();

          }

          instance.create = function (form, ticket) {

            if (form.$valid && angular.isObject(ticket.error_code)) {

              instance.data.loading = true;


              Tickets
                .create({
                  'id_clients': client.id_clients,
                  'id_error_codes': ticket.error_code.id_error_codes,
                  'description': ticket.description,
                  'estimated_service_fee': ticket.estimated_service_fee,
                  'estimated_service_fee_reasons': ticket.estimated_service_fee_reasons,
                  'type': ticket.type,
                  'status': 'opened'
                })
                .then(function (response) {

                  instance.data.loading = false;

                  swal({
                    title: "Ticket creado correctamente",
                    text: "",
                    type: "success",
                    confirmButtonText: "Continuar",
                    confirmButtonColor: "#2196F3",
                    allowOutsideClick: true
                  }, function () {

                    $modalInstance.dismiss();

                  });

                });


            } else {

              if (!angular.isObject(ticket.error_code))
                instance.data.ticket.error_code = '';

              $scope.$broadcast('show-errors-check-validity');

              swal({
                title: 'Un momento',
                text: "Favor de completar los datos obligatorios.",
                type: "error",
                confirmButtonColor: "#128f76",
                confirmButtonText: "Aceptar"
              });

            }

          }

        }
      ]
    });

  }

  $scope.filterClients = function (type, status) {
    if (type == 'status') {
      $scope.data.searchStatus.status = status;
    } else if (type == 'serial_number') {
      $scope.data.searchStatus.serial_number = status;
    } else {
      $scope.data.searchStatus.sales_agent = status;
    }
    $scope.reloadClientsTable();
  }

  $scope.reloadClientsTable = function () {
    $scope.data.dtInstance.reloadData(function () {

    }, false);
  };

  $scope.getGroup = function (id_groups) {
    for (var i in groups)
      if (groups[i].id_groups == id_groups)
        return groups[i];
    return null;
  }

  $scope.createCharge = function (client, isFirstCharge) {
    $modal.open({
      templateUrl: 'views/client-new-charge.html',
      controllerAs: 'instance',
      size: 'lg',
      backdrop: 'static',
      keyboard: false,
      resolve: {
        'client': function () {
          return client;
        }
      },
      controller: ['$scope', '$filter', '$modalInstance', 'Charges', 'client',

        function ($scope, $filter, $modalInstance, Charges, client) {

          var instance = this;
          instance.data = {
            'user': client,
            'isFirstCharge': isFirstCharge,
            'loading': false
          }

          instance.calculateTotal = function (client, months, installation) {
            var _monthly = client.monthly_fee,
              _socialRes = (client.monthly_fee / 1.16) * .007,
              _subtotal = _monthly,
              _total = 0;

            if (client.social_responsability == 1) {
              _subtotal = _monthly + _socialRes;
            }

            _total = (_subtotal * (months > 0 ? months : 0)) + 100;

            if (installation > 0) {
              _total += installation;
            }

            return _total;
          }

          instance.close = function () {
            $modalInstance.dismiss();
          }

          instance.save = function (form) {

            if (form.$valid) {

              swal({
                title: 'Generar referencia',
                text: "Se va a generar una referencia para este movimiento, ¿continuar?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#128f76",
                confirmButtonText: "SI",
                cancelButtonText: "NO"
              }, function () {

                instance.data.loading = true;

                if (isFirstCharge) {
                  Clients
                    .subscribeToPlanOffline({
                      'id_clients': client.id_clients,
                      'months_ahead': instance.data.months_ahead,
                      'type': instance.data.type
                    })
                    .then(function (response) {

                      instance.data.loading = false;
                      var _text;
                      switch (response.service) {
                        case 'OXXO':
                          _text = '<p>La referencia para pagar en <strong>OXXO</strong> es:</p><p>' + response.reference + '</p>';
                          break;

                        case 'SPEI':
                          _text = '<p>Banco: ' + response.bank + '</p><p>CALBE: ' + response.reference + '</p>'
                          break;
                      }
                      swal({
                        title: 'Referencia creada',
                        text: _text,
                        html: true,
                        type: 'success',
                        confirmButtonColor: '#128f76',
                        confirmButtonText: 'Aceptar'
                      }, function () {

                        instance.data.loading = false;
                        $modalInstance.close(response);

                      });

                    }, function () {
                      instance.data.loading = false;
                    });
                } else {
                  Charges
                    .create({
                      'id_clients': instance.data.user.id_clients,
                      'months_ahead': instance.data.months_ahead,
                      'type': instance.data.type
                    })
                    .then(function (response) {
                      var _text;
                      switch (response.service) {
                        case 'OXXO':
                          _text = '<p>La referencia para pagar en <strong>OXXO</strong> es:</p><p>' + response.reference + '</p>';
                          break;

                        case 'SPEI':
                          _text = '<p>Banco: ' + response.bank + '</p><p>CALBE: ' + response.reference + '</p>'
                          break;
                      }
                      swal({
                        title: 'Referencia creada',
                        text: _text,
                        html: true,
                        type: 'success',
                        confirmButtonColor: '#128f76',
                        confirmButtonText: 'Aceptar'
                      }, function () {

                        instance.data.loading = false;
                        $modalInstance.close(response);

                      });
                    });
                }


                /*
                Users
                    .update({
                        'id': user.id_users,
                        'name': instance.data.name,
                        'email': instance.data.email,
                        'password': user.password
                    })
                    .then(function (response) {

                        swal({
                            title: 'Cliente actualizado correctamente',
                            text: '',
                            type: 'success',
                            confirmButtonColor: '#128f76',
                            confirmButtonText: 'Aceptar'
                        }, function () {

                            instance.data.loading = false;
                            $modalInstance.close(response);

                        });

                    })
                */
              });

            } else {

              $scope.$broadcast('show-errors-check-validity');

              swal({
                title: 'Un momento',
                text: "Favor de completar los datos obligatorios.",
                type: "error",
                confirmButtonColor: "#128f76",
                confirmButtonText: "Aceptar"
              });

            }

          }
        }
      ]
    });
  }


}

ClientsController.$inject = [
  '$scope', '$modal', '$compile', '$timeout', '$filter',
  'DTOptionsBuilder', 'DTColumnBuilder', 'DTColumnDefBuilder', 'Clients', 'Charges', 'Cards', 'Contracts', 'Groups', 'groups', 'API_AGUAGENTE'
];
