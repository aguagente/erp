function InvoiceController($scope, $modal, DTOptionsBuilder, DTColumnBuilder, DTColumnDefBuilder, $compile, $timeout, API_AGUAGENTE, $window, Invoices, $filter) {

  $scope.data = {
    "withdrawals": [],
    "balance": 0,
    'dtInstance': {},
    'dtOptions': DTOptionsBuilder.newOptions()
      .withDOM('<"html5buttons"B>lTfgitp')
      .withButtons([{
        extend: 'excel',
        exportOptions: {
          columns: '.exportar'
        },
        text: 'Excel'
      },
        {
          extend: 'pdf',
          exportOptions: {
            columns: ':not(:last-child)'
          },
          text: 'PDF',
          title: 'Clientes'
        },
        {
          extend: 'print',
          exportOptions: {
            columns: ':not(:last-child)'
          },
          text: 'Imprimir',
          title: 'Clientes',
          customize: function (win) {
            $(win.document.body).addClass('white-bg');
            $(win.document.body).css('font-size', '10px');
            $(win.document.body).find('table')
              .addClass('compact')
              .css('font-size', 'inherit');
          }
        }
      ])
      .withOption('order', [])
      .withOption('createdRow', function (row, data, dataIndex) {
        $timeout(function () {
          $compile(angular.element(row).contents())($scope)
        });
      })
      .withOption('ajax', {
        url: API_AGUAGENTE.URL + '/invoices',
        type: 'GET',
        data: function (d) {
          //d.status = $scope.data.searchStatus.status;
          //d.serial_number = $scope.data.searchStatus.serial_number;
          //d.sales_agent = $scope.data.searchStatus.sales_agent;
        }
      })
      .withDataProp('data')
      .withOption('processing', true)
      .withOption('serverSide', true)
      .withPaginationType('full_numbers'),
    'dtColumns': [
      DTColumnBuilder.newColumn('id_invoices').withTitle('ID'),
      DTColumnBuilder.newColumn('invoice').withTitle('Cliente').renderWith(function (data, type, invoice) {
        return invoice.client.name;
      }),
      DTColumnBuilder.newColumn('invoice').withTitle('Rfc').renderWith(function (data, type, invoice) {
        return invoice.client.rfc;
      }),
      DTColumnBuilder.newColumn('invoice').withTitle('Fecha').renderWith(function (data, type, invoice) {
        return $filter('date')(invoice.date_stamp, ':shortDate');
      }),
      DTColumnBuilder.newColumn('invoice').withTitle('Acciones').withOption("searchable", false).withOption("orderable", false).renderWith(function (data, type, invoice) {

        var urlPdf = API_AGUAGENTE.URL + '/invoices_files/' + invoice.filename + '.pdf';
        var urlXml = API_AGUAGENTE.URL + '/invoices_files/' + invoice.filename + '.xml';
        var urlZip = API_AGUAGENTE.URL + '/invoices/download_files/' + invoice.id_invoices;

        var _html = "<div class='btn-group btn-group-lg' role='group' aria-label='Basic example'>" +
          "<a class='btn btn-primary' download target='_blank' href='" + urlPdf + "'><i class='fa fa-fw fa-file-pdf-o'></i></a>" +
          "<a class='btn btn-primary' download target='_blank' href='" + urlXml + "' ><i class='fa fa-fw fa-file-code-o'></i></a>" +
          "<a class='btn btn-primary' download target='_blank' href='" + urlZip + "' ><i class='fa fa-fw fa-download '></i></a>" +
          "<button class='btn btn-primary' ng-click='sendInvoice(" + JSON.stringify(invoice).replace(/'/g, '&#39') + ")'><i class='fa fa-fw fa-envelope'></i></button>" +
          "</div>"
        return _html;
      })
    ]
  };


  $scope.sendInvoice = function (invoice) {
    swal({
      title: "Envio de Factura",
      text: "<textarea class='form-control' id='message_invoice'>Factura para " + invoice.client.name + "</textarea>",
      html: true,
      showCancelButton: true,
      closeOnConfirm: false,
      showLoaderOnConfirm: true,
      animation: "slide-from-top",
      inputPlaceholder: "Escribe un mensaje para el cliente"
    }, function (inputValue) {
      if (inputValue === false) return false;
      if (inputValue === "") {
        swal.showInputError("Debe ingresar el mensaje para el usuario");
        return false
      }
      var message = $('#message_invoice').val();
      console.log(message);
      Invoices.send_invoice({id_invoices: invoice.id_invoices, message: message}).then(function (response) {
        swal("Enviado", "La factura fue enviada", "success");
        console.log(response);
      }, function (error) {
        console.log(response);
      });
    });
  };

  $scope.downloadFiles = function (invoice) {
    Invoices.download_files({id_invoices: invoice.id_invoices}).then(function (response) {
      console.log(response);
    }, function (error) {
      console.log(response);
    });
  };

  $scope.downloadFile = function (invoice, type) {
    var url = API_AGUAGENTE.URL + '/invoices_files/' + invoice.filename + '.' + type;
    $window.open(s);
  }

}

InvoiceController.$inject = ['$scope', '$modal', 'DTOptionsBuilder', 'DTColumnBuilder', 'DTColumnDefBuilder', '$compile', '$timeout', 'API_AGUAGENTE', '$window', 'Invoices', '$filter'];
