/**
 *
 * @param $scope
 * @param $rootScope
 * @param $state
 * @param AuthService
 * @param SessionService
 * @constructor
 */
function LoginController ($scope, $rootScope, $state, AuthService, SessionService) {

	$scope.data = {
		'email': '',
		'password': '',
		'appVersion': 1
	}

	$scope.login = function(credentials) {

		AuthService
			.login(credentials)
			.then(function (response) {

				if(response.status === 401) {

					swal({
						title: "Error!",
						text: "credenciales incorrectas, vuelve a intentarlo.",
						type: "error",
					});

				} else if(response.status === 403) {

					swal({
						title: "Error!",
						text: "Acceso denegado.",
						type: "error",
					});

				} else if(response.status === 200) {

					$rootScope.setCurrentUser(response.data.user);

					$state.go('app.main');

				}

			}, function (error) {


			});

	}

	$scope.checkSession = function () {

		var user = SessionService.getUser();

		if(user) {

			$rootScope.setCurrentUser(user);

			$state.go('app.main');

		}

	}

    $scope.checkSession();

}
LoginController.$inject = ['$scope', '$rootScope', '$state', 'AuthService', 'SessionService'];
