function ContractsController(
    $scope, $modal, $timeout, 
    DTOptionsBuilder, DTColumnDefBuilder, Clients, Contracts, Cards, 
    contracts) {

	$scope.data = {
		'contracts' : contracts,
		'dtOptions': DTOptionsBuilder.newOptions()
	        .withDOM('<"html5buttons"B>lTfgitp')
	        .withButtons([
	            {
	            	extend: 'excel',
	            	exportOptions: {
			          columns: ':not(:last-child)'
			        },
			        text: 'Excel'
	            },
	            {
	            	extend: 'pdf', 
	            	exportOptions: {
			          columns: ':not(:last-child)'
			        },
			        text: 'PDF',
                    title: 'Contratos'
	            },
	            {
	            	extend: 'print',
	            	exportOptions: {
			          columns: ':not(:last-child)'
			        },
			        text: 'Imprimir',
                    title: 'Contratos',
	                customize: function (win){
	                    $(win.document.body).addClass('white-bg');
	                    $(win.document.body).css('font-size', '10px');

	                    $(win.document.body).find('table')
	                        .addClass('compact')
	                        .css('font-size', 'inherit');
	                }
	            }
	        ])
            .withOption('order', []),
		'dtColumnDefs': [
	        DTColumnDefBuilder.newColumnDef(10).notSortable(),
            DTColumnDefBuilder.newColumnDef(3).withOption("type", "date-nl"),
            DTColumnDefBuilder.newColumnDef(6).withOption("type", "date-nl")
	    ]
	}

    $scope.reviewImages = function(contract) {
        
        $('#blueimp-gallery').data('use-bootstrap-modal', true);

        blueimp.Gallery( 
            (function(documents) {
                var images = [];
                for(var i in documents) {
                    images.push({
                        'href': documents[i]
                    });
                }
                return images;
            })(contract.documents),
            $('#blueimp-gallery').data()
        );
            
    }

    $scope.reviewDetails = function(contract) {

    	$modal.open({

            templateUrl: 'views/client-review-purchase-contract.html',
            controllerAs: 'instance',
            size: 'lg',
            backdrop: 'static',
            keyboard: false,
            resolve: { 

                client: function() {

                    return contract.client;

                },

                contract: function() {

                    return contract;

                },

                cards: function() {

                    return Cards
                        .getAll({
                            'id_clients': contract.id_clients
                        })
                        .then(function(response) {
                            return response;
                        })

                }

            },
            controller: ['$filter', '$modalInstance', 'client', 'contract', 'cards',
            
                function($filter, $modalInstance, client, contract, cards) {
                    console.log(cards);
                    client.cards = cards;
                    
                    var instance = this;

                    instance.data = {
                        'client': client,
                        'contract': contract,
                        'editable': false
                    }

                    instance.close = function() {

                        $modalInstance.dismiss();

                    }

                }
            ]
        })
        .result.then(function (changedClient) {
            
            $scope.data.clients[$scope.data.clients.indexOf(client)] = changedClient;

        })
        .catch(function() {
        });

    }

}
ContractsController.$inject = [
    '$scope', '$modal', '$timeout',
    'DTOptionsBuilder', 'DTColumnDefBuilder', 'Clients', 'Contracts', 'Cards',
    'contracts'
];