/**
 * Controlador de pools
 * 
 * @param {any} $scope 
 * @param {any} $modal 
 * @param {any} DTOptionsBuilder 
 * @param {any} DTColumnDefBuilder 
 * @param {any} Polls 
 * @param {any} polls 
 */
function PollsController(
	$scope, $modal,
	DTOptionsBuilder, DTColumnDefBuilder, Polls,
	polls) {

	$scope.data = {
		'polls' : polls,
		'dtOptions': DTOptionsBuilder.newOptions()
	        .withDOM('<"html5buttons"B>lTfgitp')
	        .withButtons([
	            {
	            	extend: 'excel',
	            	exportOptions: {
			          columns: ':not(:last-child)'
			        },
			        text: 'Excel'
	            },
	            {
	            	extend: 'pdf', 
	            	exportOptions: {
			          columns: ':not(:last-child)'
			        },
			        text: 'PDF',
                    title: 'Encuestas'
	            },
	            {
	            	extend: 'print',
	            	exportOptions: {
			          columns: ':not(:last-child)'
			        },
			        text: 'Imprimir',
                    title: 'Encuestas',
	                customize: function (win){
	                    $(win.document.body).addClass('white-bg');
	                    $(win.document.body).css('font-size', '10px');

	                    $(win.document.body).find('table')
	                        .addClass('compact')
	                        .css('font-size', 'inherit');
	                }
	            }
	        ])
            .withOption('order', []),
		'dtColumnDefs': [
	        DTColumnDefBuilder.newColumnDef(3).notSortable(),
            DTColumnDefBuilder.newColumnDef(1).withOption("type", "date-nl"),
            DTColumnDefBuilder.newColumnDef(2).withOption("type", "date-nl")
	    ]
	
	}

	$scope.add = function() {

		$modal.open({

            templateUrl: 'views/polls-save.html',
            controllerAs: 'instance',
            size: 'lg',
            backdrop: 'static',
            keyboard: false,
            resolve: { },
            controller: ['$scope', '$timeout', '$filter', '$modalInstance', 'Polls',
            
                function($scope, $timeout, $filter, $modalInstance, Polls) {

                    var instance = this;

                    instance.data = {
                        'poll': {
                        	'name': '',
                        	'start_date': null,
                        	'end_date': null,
                        	'questions': []
                        },
                        'datepicker_inicio' : false,
  						'datepicker_fin' : false,
  						'loading': false
                    }

                    instance.close = function() {

                        $modalInstance.dismiss();

                    }

                    instance.open = function($event, datepicker) {

					    $event.preventDefault();
					    $event.stopPropagation();

					    instance.data[datepicker] = !instance.data[datepicker];
				  	}

				  	instance.addQuestion = function() {

				  		instance.data.poll.questions.push({
				  			'description': '',
                            'type': 'single',
				  			'options': []
				  		})

				  	}

				  	instance.deleteQuestion = function($event, question) {

				  		instance.data.poll.questions.splice(
				  			instance.data.poll.questions.indexOf(question),
				  			1
			  			);

				  	}

				  	instance.addOption = function(question) {

				  		question.options.push({
				  			'description': ''
				  		})

				  	}

				  	instance.deleteOption = function(question, option) {

				  		question.options.splice(
				  			question.options.indexOf(option),
				  			1
			  			);

				  	}

				  	instance.save = function(form, poll) {

                        if(form.$valid) {

                            instance.data.loading = true;

                            Polls
                                .create(poll)
                                .then(function(response) {

                                    swal({
                                        title: 'Encuesta creada correctamente',
                                        text: '',   
                                        type: 'success',    
                                        confirmButtonColor: '#128f76',   
                                        confirmButtonText: 'Aceptar'
                                    }, function() {

                                        instance.data.loading = false;
                                        $modalInstance.close(response);
                                        
                                    });

                                }, function(err) {

                                	instance.data.loading = false;

                                })
                            
                        } else {

                        	$scope.$broadcast('show-errors-check-validity');

                            swal({
                                title: 'Un momento',
                                text: "Favor de completar los datos obligatorios.",   
                                type: "error",    
                                confirmButtonColor: "#128f76",   
                                confirmButtonText: "Aceptar"
                            });
                            
                        }

                    }

                }
            ]
        })
        .result.then(function (poll) {

        	$scope.data.polls.push(poll);
        })
        .catch(function() {
        });
	
	}

	$scope.view = function(poll) {

		poll.loading = true;

		$modal.open({

            templateUrl: 'views/polls-review-questions.html',
            controllerAs: 'instance',
            size: 'lg',
            backdrop: 'static',
            keyboard: false,
            resolve: { 

                poll: function() {

                    return Polls
                    	.get(poll)
                    	.then( function(response) {
                    		return response;
                    	});

                }

            },
            controller: ['$filter', '$modalInstance', 'poll',
            
                function($filter, $modalInstance, poll) {

                    var instance = this;

                    instance.data = {
                        'poll': poll
                    }

                    instance.close = function() {

                        $modalInstance.dismiss();

                    }

                }
            ]
        })
        .result.then(function () {
            poll.loading = false;
        })
        .catch(function() {
        	poll.loading = false;
        });

	}

	$scope.edit = function(poll) {

		poll.loading = true;

		$modal.open({

            templateUrl: 'views/polls-save.html',
            controllerAs: 'instance',
            size: 'lg',
            backdrop: 'static',
            keyboard: false,
            resolve: { 
            	poll: function() {

            		return Polls
                    	.get(poll)
                    	.then( function(response) {
                    		return response;
                    	});

            	}


            },
            controller: ['$scope',  '$timeout', '$filter', '$modalInstance', 'poll',
            
                function($scope, $timeout, $filter, $modalInstance, poll) {

                    var instance = this;


                    instance.data = {
                        'poll': poll,
                        'datepicker_inicio' : false,
  						'datepicker_fin' : false,
  						'loading': false
                    }

                    instance.close = function() {

                        $modalInstance.dismiss();

                    }

                    instance.open = function($event, datepicker) {

					    $event.preventDefault();
					    $event.stopPropagation();

					    instance.data[datepicker] = !instance.data[datepicker];
				  	}

				  	instance.addQuestion = function() {

				  		instance.data.poll.questions.push({
				  			'description': '',
				  			'options': []
				  		})

				  	}

				  	instance.deleteQuestion = function($event, question) {

				  		instance.data.poll.questions.splice(
				  			instance.data.poll.questions.indexOf(question),
				  			1
			  			);

				  	}

				  	instance.addOption = function(question) {

				  		question.options.push({
				  			'description': ''
				  		})

				  	}

				  	instance.deleteOption = function(question, option) {

				  		question.options.splice(
				  			question.options.indexOf(option),
				  			1
			  			);

				  	}

				  	instance.save = function(form, poll) {

                        if(form.$valid) {

                            instance.data.loading = true;

                            Polls
                                .update(poll)
                                .then(function(response) {

                                    swal({
                                        title: 'Encuesta editada correctamente',
                                        text: '',   
                                        type: 'success',    
                                        confirmButtonColor: '#128f76',   
                                        confirmButtonText: 'Aceptar'
                                    }, function() {

                                        instance.data.loading = false;
                                        $modalInstance.close(response);
                                        
                                    });

                                }, function(err) {

                                	instance.data.loading = false;

                                });

                            
                        } else {

                        	$scope.$broadcast('show-errors-check-validity');

                            swal({
                                title: 'Un momento',
                                text: "Favor de completar los datos obligatorios.",   
                                type: "error",    
                                confirmButtonColor: "#128f76",   
                                confirmButtonText: "Aceptar"
                            });
                            
                        }

                    }

                }
            ]
        })
        .result.then(function (editedPoll) {

        	$scope.data.polls[$scope.data.polls.indexOf(poll)] = editedPoll;

        	poll.loading = false;

        })
        .catch(function() {

        	poll.loading = false;

        });

	}

    $scope.reviewAnswers = function(poll) {

        poll.loading = true;

        $modal.open({

            templateUrl: 'views/polls-review-client-answers.html',
            controllerAs: 'instance',
            size: 'lg',
            backdrop: 'static',
            keyboard: false,
            resolve: { 

                poll: function() {

                    return poll;

                },

                answers: function() {

                    return Polls
                        .getAnswers(poll)
                        .then(function(response) {
                            return response;
                        })

                }

            },
            controller: ['$scope', '$timeout', '$filter', '$modalInstance', 'poll', 'answers',
            
                function($scope, $timeout, $filter, $modalInstance, poll, answers) {

                    var instance = this;

                    instance.data = {
                        'poll': poll,
                        'questions': $filter('groupBy')(answers, 'question'),
                        'loading': false,
                        'answers': [],
                        'totalAnswers': 0,
                        'chart': {
                            title: { text: '' },
                            subtitle: { text: '' },
                            series: [{
                                name: 'Preguntas',
                                colorByPoint: true,
                                data: []
                            }],
                            options: {
                                chart: {
                                    type: 'pie',
                                    events: {}
                                },
                                plotOptions: {
                                    pie: {
                                        allowPointSelect: true,
                                        cursor: 'pointer',
                                        dataLabels: {
                                            enabled: true,
                                            formatter: function() {
                                                return '<span style="font-size:16px; color: #23b7e5;font-weight: bold;margin-right:5px;"># '+(this.series.data.indexOf(this.point) + 1)+'   </span>' + $filter('currency')(this.point.percentage, '', 2) + ' %';
                                            }
                                        },
                                        showInLegend: true
                                    }
                                },
                                legend: {
                                    itemStyle: {
                                        fontWeight: 'normal'
                                    }
                                },
                                credits: {
                                    enabled: false
                                },
                                tooltip: {
                                    pointFormat: '<b>{point.percentage:.1f}%</b>'
                                }
                            }

                        }
                    }

                    instance.close = function() {

                        $modalInstance.dismiss();

                    }

                    instance.reviewChart = function(question, answers) {

                        var groupedAnswers = $filter('groupBy')($filter('flatten')($filter('map')(answers, 'answers')), 'answer');

                        instance.data.answers = groupedAnswers;
                        instance.data.totalAnswers = ($filter('flatten')($filter('map')(answers, 'answers'))).length

                        var data = [];

                        for(var i in groupedAnswers) {

                            data.push({
                                'name': i,
                                'y': groupedAnswers[i].length
                            });

                        }
                        
                        instance.data.chart.title.text = question;
                        instance.data.chart.series[0].data = data;

                    }

                }
            ]
        })
        .result.then(function () {
            poll.loading = false;
        })
        .catch(function() {
            poll.loading = false;
        });


    }

	$scope.delete = function(poll) {


		swal({   
			title: "¿Estás seguro?",   
			text: "La encuesta ya no estará disponible",   
			type: "warning",   
			showCancelButton: true,   
			confirmButtonColor: "#DD6B55",   
			confirmButtonText: "Eliminar",   
			cancelButtonText: "Cancelar",   
			closeOnConfirm: false,   
			closeOnCancel: true 
		}, function(isConfirm){  


			if (isConfirm) {

				poll.loading = true;
				Polls
					.delete(poll)
					.then(function(){
						$scope.data.polls.splice(
				  			$scope.data.polls.indexOf(poll),
				  			1
			  			);
						swal("Encuesta eliminada!", "", "success");
						poll.loading = false;

					})  

			}

		});

	}

}

PollsController.$inject = [
	'$scope', '$modal',
	'DTOptionsBuilder', 'DTColumnDefBuilder', 'Polls',
	'polls'
];