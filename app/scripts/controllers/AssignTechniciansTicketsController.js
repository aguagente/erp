function AssignTechniciansTicketsController(
	$scope, Users, technicians, Tickets, tickets,
    $filter, $timeout, $compile, $sce, $templateRequest) {

	$scope.data = {
		'technicians' : technicians,
        'techniciansSelectedList' : [],
		'filteredTickets': null,
        'currentNumberOfColumns': -1,
        'choosenTechnicians': null
    };

    $scope.columnData = [];
    $scope.hiddenColumns = [];

    filterTickets = function(tickets) {
    	$timeout(function(){
	    	$scope.data.filteredTickets = $filter('filter')(tickets, {'status': 'opened'});
            $scope.data.choosenTechnicians = Array($scope.data.filteredTickets.length);
            $scope.data.choosenTechnicians.fill('');
    	});
    };

    selectedTechnicianChanged = function(newSelectedTechnician, oldSelectedTechnician) {
        if($scope.data.techniciansSelectedList.length > 0) {
            $scope.data.techniciansSelectedList = $filter('filter')($scope.data.techniciansSelectedList, {id: "!" + oldSelectedTechnician.id});
        }
        $scope.data.techniciansSelectedList.push(newSelectedTechnician);
        for (var i = 0; i < $scope.data.choosenTechnicians.length; i++) {
            $scope.data.choosenTechnicians[i] = $scope.data.techniciansSelectedList[0];
        }
    };

    sliceTechnician = function(lastColumnId, newColumnId) {
        $scope.columnData[lastColumnId].isDisabled = true;
        return $filter('filter')($scope.columnData[lastColumnId].technicians, {id: "!" + $scope.columnData[lastColumnId].selectedTechnician.id});
    };

    /**
      * Return a new array of technicians without the one selected in the lastColumnId
      */
    lockTechnician = function(lastColumnId, newColumnId) {
        return (lastColumnId < 0) ? $scope.data.technicians: sliceTechnician(lastColumnId, newColumnId);
    };

    $scope.desassignTicket = function(ticket, ticketIndex, columnId) {
        $scope.columnData[columnId].myTickets.splice(ticketIndex, 1);
        // agregar el ticket al arreglo de filteredTickets
        $scope.data.filteredTickets.push(ticket);
    };

    $scope.assignTicket = function(ticket, ticketIndex, choosenTechnician) {
        $scope.data.filteredTickets.splice(ticketIndex, 1);
        // encontrar el indice de la columna que tiene seleccionado a choosenTechnician
        $scope.columnData.forEach(function(columna, index) {
            if (columna.selectedTechnician === choosenTechnician) {
                $scope.columnData[index].myTickets.push(ticket);
            }
        });
    };

    $scope.deleteColumn = function(indexColumn) {
        var parentColumn = angular.element( document.querySelector( '#parent-assign-column' ) )[0];
        parentColumn.childNodes.forEach(function(node) {
            if (node.nodeType === 3) {
                // it is a text node type, we can safely remove it
                node.remove();
            }
        });
        parentColumn.childNodes[indexColumn + 1].style.display = "none";
        $scope.hiddenColumns.push(parentColumn.childNodes[indexColumn + 1]);
    };

    $scope.addNewColumn = function(lastColumnId, newColumnId) {
        // is there a hidden column? yes? then show it and exit;
        if ($scope.hiddenColumns.length > 0) {
            $scope.hiddenColumns.pop().style.display = "initial";
            return;
        }

        // before openning a new column, we must lock the selected technician
        var myNewTechnicianList = lockTechnician(lastColumnId, newColumnId);
        $scope.columnData.push(
            {
                technicians: myNewTechnicianList,
                date: null,
                selectedTechnician: myNewTechnicianList[0],
                isDisabled: false,
                myTickets: []
            }
        );

        $scope.$watch(
            'columnData[' + ($scope.columnData.length - 1) + '].selectedTechnician', selectedTechnicianChanged);

        var templateUrl = $sce.getTrustedResourceUrl('views/asignedTicketsToTechnician.html');

        $templateRequest(templateUrl).then(function(template) {
            // template is the HTML template as a string
            var column = angular.element( template.replace(/__INDEX_COLUMN__/g, newColumnId.toString()) )[0];
            var lastColumn = angular.element( document.querySelector( '#last-assign-column' ) )[0];
            var parentColumn = angular.element( document.querySelector( '#parent-assign-column' ) )[0];
            $compile(column)($scope);
            parentColumn.insertBefore(column, lastColumn);

            // add +1 to column counter
            $scope.data.currentNumberOfColumns += 1;
        }, function() {
            // An error has occurred
        });
    };

    tickets.forEach(function(ticket){
    	ticket.showBasicInfo = true;
    });

    filterTickets(tickets);
}
AssignTechniciansTicketsController.$inject = [
	'$scope', 'Users', 'technicians', 'Tickets', 'tickets',
    '$filter', '$timeout', '$compile', '$sce', '$templateRequest'
];