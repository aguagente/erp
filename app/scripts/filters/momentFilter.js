function momentFilter() {

	return function (input, momentFn /*, param1, param2, ...param n */) {
	    var args = Array.prototype.slice.call(arguments, 2),
	        momentObj = moment(input);
	        if(momentFn === 'utc') {

	        	return moment.utc(input);

	        } else if(momentFn === 'unix') {

				return moment.unix(input);

		    } else {

	    		return momentFn ? momentObj[momentFn].apply(momentObj, args) : momentObj;

	        }
  	};

}

function secondsToDate() {
	return function(seconds) {
	    return new Date(1970, 0, 1).setSeconds(seconds);
	};
}
