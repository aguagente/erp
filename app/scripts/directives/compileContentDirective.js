function compileContent($compile, watchDom) {

	return {

		link: function($scope, element, attrs) {

			watchDom.$watch(element[0], function (newValue, oldValue) {

				$compile(element.contents())($scope);

			});

		}

	}

}
compileContent.$inject = ['$compile', 'watchDom'];